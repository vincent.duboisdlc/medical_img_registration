#!usr/bin/env python

import os
import cv2
import reglib as reg
import tkinter as tk
from tkinter import ttk
import numpy as np
from PIL import ImageTk,Image
import matplotlib.pyplot as plt

loop = [False]
focus = [0]
h0 = [None]


def clean_tmp():
  if os.path.exists('./data/tmp/US0.jpg'):
    os.system('rm -f ./data/tmp/US0.jpg')
  if os.path.exists('./data/tmp/H0.npy'):
    os.system('rm -f ./data/tmp/H0.npy')
  if os.path.exists('./data/tmp/RMI.jpg'):
    os.system('rm -f ./data/tmp/RMI.jpg')

def launch_calib():
  loop[0] = False
  clean_tmp()
  mat[3] = mat[0]
  cv2.imwrite('./data/tmp/US0.jpg', mat[3])
  mat[1] = mat_nosig
  mat[2] = mat_nosig
  h0[0] = None
  os.system('python3 calib.py')


def update_img():
  mat[0] = cv2.imread('./data/tmp/US.jpg') 
  tkimg[0] = ImageTk.PhotoImage(Image.fromarray(mat[0]))
  tkimg[1] = ImageTk.PhotoImage(Image.fromarray(mat[1]))
  tkimg[2] = ImageTk.PhotoImage(Image.fromarray(mat[2]))
  tkimg_mini[0] = ImageTk.PhotoImage(Image.fromarray(mat[0]).resize((266, 200), Image.ANTIALIAS))
  tkimg_mini[1] = ImageTk.PhotoImage(Image.fromarray(mat[1]).resize((266, 200), Image.ANTIALIAS))
  tkimg_mini[2] = ImageTk.PhotoImage(Image.fromarray(mat[2]).resize((266, 200), Image.ANTIALIAS))
  main_frame.config(image=tkimg[focus[0]])
  mini_US.config(image=tkimg_mini[0])
  mini_RMI.config(image=tkimg_mini[1])
  mini_reg.config(image=tkimg_mini[2])
  root.update_idletasks()


def change_main_frame(event):
  mini = str(event.widget)
  if mini == '.usmini':
    focus[0] = 0
    mini_US.config(highlightbackground="red")
    mini_RMI.config(highlightbackground="black")
    mini_reg.config(highlightbackground="black")
  elif mini == '.rmimini':
    focus[0] = 1
    mini_US.config(highlightbackground="black")
    mini_RMI.config(highlightbackground="red")
    mini_reg.config(highlightbackground="black")
  elif mini == '.regmini':
    focus[0] = 2
    mini_US.config(highlightbackground="black")
    mini_RMI.config(highlightbackground="black")
    mini_reg.config(highlightbackground="red")
  update_img()


def init():
  if os.path.exists('./data/tmp/RMI.jpg') and os.path.exists('./data/tmp/H0.npy'):
    mat[1] = cv2.imread('./data/tmp/RMI.jpg')
    h0[0] = np.load('./data/tmp/H0.npy')
    loop[0] = True
    focus[0] = 2
    

def full_registration():
  mat_reg_temp, mat_matches, h = reg.full_basic_registration(mat[3], mat[0])
  height, width, layers = mat[1].shape
  mat[2] = cv2.warpPerspective(mat_reg_temp, h0[0], (width, height))

def main_loop():
  if loop[0] == True:
    full_registration() 
  else:
    init()
  update_img()
  root.after(500, main_loop)
 


root = tk.Tk()
root.title('US-RMI registration')
root.configure(bg='black')


#Images
mat_nosig = cv2.imread('./data/interface/nosignal.jpg')
mat = [cv2.imread('./data/tmp/US.jpg'),mat_nosig, mat_nosig, mat_nosig]
tkimg = [None, None, None] 
tkimg_mini = [None, None, None]

#Main frame
main_frame = tk.Label(root, name='main_frame')
main_frame.grid(row=0, column=0, columnspan=4)

#Miniatures
mini_US = tk.Label(root, name='usmini', relief='solid', highlightbackground="red", highlightthickness=4)
mini_US.grid(row=1, column=0, rowspan=2)
mini_RMI = tk.Label(root, name='rmimini', relief='solid', highlightbackground="black", highlightthickness=4)
mini_RMI.grid(row=1, column=1, rowspan=2)
mini_reg = tk.Label(root, name='regmini', relief='solid', highlightbackground="black", highlightthickness=4)
mini_reg.grid(row=1, column=2, rowspan=2)

#Titles
title_US = tk.Label(root, text='US', bg='black', fg='white', borderwidth=0, relief="flat", highlightbackground="black", highlightcolor="black", highlightthickness=0, bd= 0).grid(row=3,column=0)
title_RMI = tk.Label(root, text='RMI', bg='black', fg='white', borderwidth=0, relief="flat", highlightbackground="black", highlightcolor="black", highlightthickness=0, bd= 0).grid(row=3,column=1)
title_reg = tk.Label(root, text='registered US', bg='black', fg='white', borderwidth=0, relief="flat", highlightbackground="black", highlightcolor="black", highlightthickness=0, bd= 0).grid(row=3,column=2)

#Button Style
style = ttk.Style() 
style.configure("W.TButton", 
                width=120,
                height=75,
                bg='black',
                fg='black',
                borderwidth=0,
                relief="flat",
                highlightbackground="black",
                highlightcolor="black",
                highlightthickness=0,
                bd= 0)
 
#Buttons
icon_calib = ImageTk.PhotoImage(Image.open('./data/interface/calib2.png').resize((50, 50), Image.ANTIALIAS))
icon_quit = ImageTk.PhotoImage(Image.open('./data/interface/quit2.png').resize((50, 50), Image.ANTIALIAS))
button_calib = ttk.Button(root, text="Calib", image=icon_calib, style="W.TButton", command=launch_calib)
button_calib.grid(row=1, column=3)
button_quit = ttk.Button(root, text="Quit", image=icon_quit, style="W.TButton", command=root.quit)
button_quit.grid(row=2, column=3)

clean_tmp()
root.bind("<Button 1>", change_main_frame)
main_loop()
root.mainloop()
