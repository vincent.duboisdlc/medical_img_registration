from __future__ import print_function
import Registration
import matplotlib.pyplot as plt
from utils.utils import *
import os
import cv2


folder_path = '../img/test-IRM/'
tgt_filename = 'IRM3.jpg'
src_filename = 'IRM3_distorted1.jpg'
output_folder_path = '../img/results/cnn-' + src_filename[0:-4]

def create_output(output_folder_path, img_tgt, img_src, img_reg, save_matches=False, img_matches=None):
  if(os.path.exists(output_folder_path)):
    os.system('rm -rf ' + output_folder_path + '/*')
  else:
    os.system('mkdir ' + output_folder_path)
  cv2.imwrite(output_folder_path+'/img_src.jpg', img_src)
  cv2.imwrite(output_folder_path+'/img_tgt.jpg', img_tgt)
  cv2.imwrite(output_folder_path+'/img_reg.jpg', img_reg)
  if (save_matches):
    cv2.imwrite(output_folder_path+'/img_matches.jpg', img_matches)


def main():
  img_tgt = cv2.imread(folder_path+tgt_filename)
  img_src = cv2.imread(folder_path+src_filename)

  #initialize
  reg = Registration.CNN()
  #register
  X, Y, Z = reg.register(img_tgt, img_src)
  #generate regsitered image using TPS
  img_reg = tps_warp(Y, Z, img_src, img_tgt.shape)
  cb = checkboard(img_tgt, img_reg, 11)

  #create_output(output_folder_path,img_tgt,img_src,img_reg)

  plt.subplot(221)
  plt.title('Target')
  plt.imshow(cv2.cvtColor(img_tgt, cv2.COLOR_BGR2RGB))
  plt.subplot(222)
  plt.title('Source')
  plt.imshow(cv2.cvtColor(img_src, cv2.COLOR_BGR2RGB))
  plt.subplot(223)
  plt.title('Registered')
  plt.imshow(cv2.cvtColor(img_reg, cv2.COLOR_BGR2RGB))
  #plt.subplot(224)
  #plt.title('Checkboard')
  #plt.imshow(cv2.cvtColor(cb, cv2.COLOR_BGR2RGB))
  plt.show()


if __name__ == '__main__':
  main()
