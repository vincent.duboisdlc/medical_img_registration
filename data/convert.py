#!/usr/bin/env python

import os
import cv2
import reglib as reg

folder_path = './img/CoBra5/MRI_anon/'

for filename in os.listdir(folder_path):
  if filename.endswith(".dcm") and filename.startswith("MR"):
    dcm_path = folder_path + filename
    jpg_path = dcm_path[0:-4] + '.jpg'
    if(os.path.exists(jpg_path)):
      os.system('rm -rf ' + jpg_path)
    print(dcm_path)
    reg.dicom_to_jpg(dcm_path)
