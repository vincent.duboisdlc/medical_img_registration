#!usr/bin/env python

import os
import cv2
import reglib as reg
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
import numpy as np
from PIL import ImageTk,Image
import matplotlib.pyplot as plt

n_kp = 4
kp_src = []
kp_tgt = []
matches = []


def clear_box(box):
  box.delete (0, tk.END)

def manual_transform():
  points1 = np.zeros((len(kp_src), 2), dtype=np.float32)
  points2 = np.zeros((len(kp_tgt), 2), dtype=np.float32)
  for i in range(0,len(kp_tgt)):
    points1[i, :] = kp_src[i].pt
    points2[i, :] = kp_tgt[i].pt
  h0, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
  print("Estimated homography : \n",  h0)
  np.save("./data/tmp/H0.npy", h0)
  cv2.imwrite("./data/tmp/RMI.jpg", mat[1])
  calib.quit()


def get_click(event):
  zone = str(event.widget)
  if zone != '.matches_window':
    return
  x = event.x
  y = event.y
  #print(x,y)
  if len(kp_src) == len(kp_tgt):
    if x < src_width:
      kp_src.append(cv2.KeyPoint(x, y, 1))
      box.insert(tk.END, '> Keypoint ('+str(x)+','+str(y)+') successfully added on US, select keypoint on RMI')
  elif len(kp_src) > len(kp_tgt):
    if x >= src_width:
      calib.unbind("<Button 1>")
      kp_tgt.append(cv2.KeyPoint(x-src_width, y, 1))
      matches.append(cv2.DMatch(len(kp_src)-1,len(kp_tgt)-1,0))
      clear_box(box)
      box.insert(tk.END, '> '+str(len(matches))+' matches created, need at least '+str(n_kp)+' to compute registration')
  print_matches()
  manage_buttons()


def add_keypoint():
  calib.bind("<Button 1>", get_click)
  box.insert(tk.END, '> Select keypoint on US')


def remove_keypoint():
  if len(kp_src) <= 0:
    return
  if len(kp_src) == len(kp_tgt):
    del kp_tgt[-1]
    del matches[-1]
    clear_box(box)
    box.insert(tk.END, '> Select new keypoint on RMI')
    calib.bind("<Button 1>", get_click)
  elif len(kp_src) > len(kp_tgt):
    calib.unbind("<Button 1>")
    del kp_src[-1]
    clear_box(box)
    box.insert(tk.END, '> '+str(len(matches))+' matches created, need at least '+str(n_kp)+' to compute registration')
  print_matches()
  manage_buttons()


def print_matches():
  mat_matches = cv2.drawMatches(mat[0], kp_src, mat[1], kp_tgt, matches, None) 
  img_matches = Image.fromarray(mat_matches)
  tkimg_matches[0] = ImageTk.PhotoImage(img_matches)
  label_matches.config(image=tkimg_matches[0])
  calib.update_idletasks()
 

def manage_buttons():
  if len(kp_src) > 0:
    button_canc.config(state=tk.ACTIVE)
  else:
    button_canc.config(state=tk.DISABLED)
  if len(kp_src) == len(kp_tgt) and len(kp_src) >= n_kp:
    button_done.config(state=tk.ACTIVE)
  else:
    button_done.config(state=tk.DISABLED)
  calib.update_idletasks()


def openRMI():
  path = filedialog.askopenfilename(initialdir='./data/test-IRM/', title='Choose RMI layer', filetypes=[("jpg files","*.jpg")])
  mat[1] = cv2.imread(path)
  print_matches()
  button_add.config(state=tk.ACTIVE)
  calib.update_idletasks()


calib = tk.Tk()
calib.title('Calibration')
calib.configure(bg='black')

#Style
style = ttk.Style() 
style.configure("W.TButton", 
                width=120,
                height=75,
                bg='black',
                fg='black',
                borderwidth=0,
                relief="flat",
                highlightbackground="black",
                highlightcolor="black",
                highlightthickness=0,
                bd= 0)
 
#Images
mat = [cv2.imread('./data/tmp/US0.jpg'), cv2.imread('./data/interface/nosignal2.jpg')]
src_height, src_width, layers = mat[0].shape
tkimg_matches = [None] 
label_matches = tk.Label(calib, name='matches_window')
label_matches.grid(row=0, column=0, columnspan=6)
print_matches()

#Instruction boxe
box = tk.Listbox(calib, 
                width=130, 
                height=5, 
                bg='black', 
                fg='white', 
                borderwidth=0, 
                relief="flat", 
                highlightbackground="black", 
                highlightcolor="black", 
                highlightthickness=0, 
                bd= 0, 
                selectbackground='black')
box.insert(tk.END, '> '+str(len(matches))+' matches created, need at least '+str(n_kp)+' to compute registration')
box.grid(row=1, column=0)

#Buttons
icon_load = ImageTk.PhotoImage(Image.open('./data/interface/load2.png').resize((50, 50), Image.ANTIALIAS))
icon_add = ImageTk.PhotoImage(Image.open('./data/interface/add2.png').resize((50, 50), Image.ANTIALIAS))
icon_canc = ImageTk.PhotoImage(Image.open('./data/interface/canc2.png').resize((50, 50), Image.ANTIALIAS))
icon_quit = ImageTk.PhotoImage(Image.open('./data/interface/quit2.png').resize((50, 50), Image.ANTIALIAS))
icon_done = ImageTk.PhotoImage(Image.open('./data/interface/done2.png').resize((50, 50), Image.ANTIALIAS))
button_load = ttk.Button(calib, text="Load", image=icon_load, style="W.TButton", command=openRMI)
button_load.grid(row=1, column=1)
button_add = ttk.Button(calib, text="Add", image=icon_add, style="W.TButton", state=tk.DISABLED, command=add_keypoint)
button_add.grid(row=1, column=2)
button_canc = ttk.Button(calib, text="Cancel", image=icon_canc, style="W.TButton", state=tk.DISABLED, command=remove_keypoint)
button_canc.grid(row=1, column=3)
button_done = ttk.Button(calib, text="Done", image=icon_done, style="W.TButton", state=tk.DISABLED, command=manual_transform)
button_done.grid(row=1, column=4)
button_quit = ttk.Button(calib, text="Quit", image=icon_quit, style="W.TButton", command=calib.quit)
button_quit.grid(row=1, column=5)


if __name__ == "__main__":
  calib.mainloop()
