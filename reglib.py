#!/usr/bin/env python

from __future__ import print_function
import numpy as np
import pydicom
import matplotlib.pyplot as plt
import os
import cv2


#load jpg/png image as matrix
def load_img(path, gray=True):
  img = cv2.imread(path)
  if img is None:
    print('Could not open or find the image:', path)
    exit(0)
  if gray:
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  return img


#convert dcm file to jpg image
def dicom_to_jpg(path):
  if(path[-4:]!='.dcm'):
    print('Wrong type of file, must be ".dcm":')
    exit(0)
  dcm = pydicom.dcmread(path).pixel_array 
  if dcm is None:
    print('Could not open:', path)
    exit(0)
  #plt.imshow(dcm, cmap='gray')
  #plt.show()
  cv2.imwrite(path[0:-4]+'.jpg',dcm)


def print_output(img_tgt, img_src, img_reg, print_matches, img_matches):
  if print_matches:
    plt.subplot(121)
    plt.title('Matches')
    plt.imshow(img_matches, cmap='gray')
    plt.subplot(122)
    plt.title('Registered')
    plt.imshow(img_reg, cmap='gray')
  else:
    plt.subplot(131)
    plt.title('Target')
    plt.imshow(img_tgt, cmap='gray')
    plt.subplot(132)
    plt.title('Source')
    plt.imshow(img_src, cmap='gray')
    plt.subplot(133)
    plt.title('Registered')
    plt.imshow(img_reg, cmap='gray')
  plt.show()


#save output matrix as image
def create_output(output_folder_path, img_tgt, img_src, img_reg, save_matches, img_matches):
  if(os.path.exists(output_folder_path)):
    os.system('rm -rf ' + output_folder_path + '/*')
  else:
    os.system('mkdir ' + output_folder_path)
  cv2.imwrite(output_folder_path+'/img_src.jpg', img_src)
  cv2.imwrite(output_folder_path+'/img_tgt.jpg', img_tgt)
  cv2.imwrite(output_folder_path+'/img_reg.jpg', img_reg)
  if (save_matches):
    cv2.imwrite(output_folder_path+'/img_matches.jpg', img_matches)


# detecte and describe keypoints using ORB algorithm
def orb(img, Npoints=50):
  orb = cv2.ORB_create(Npoints)
  kp, des = orb.detectAndCompute(img, None)
  return kp, des


# matche keypoints using Brute Force and Hamming distance calcultion
def matcher(des1,des2):
  matcher = cv2.DescriptorMatcher_create(cv2.DESCRIPTOR_MATCHER_BRUTEFORCE_HAMMING)
  matches = matcher.match(des1, des2, None)
  matches = sorted(matches, key = lambda x:x.distance)
  return matches


# register image using RANSAC keypoint selector and Homography
def transform(img_tgt, img_src, kp_tgt, kp_src, matches):
  points1 = np.zeros((len(matches), 2), dtype=np.float32)
  points2 = np.zeros((len(matches), 2), dtype=np.float32)
  for i, match in enumerate(matches):
    points1[i, :] = kp_src[match.queryIdx].pt
    points2[i, :] = kp_tgt[match.trainIdx].pt
  h, mask = cv2.findHomography(points1, points2, cv2.RANSAC)
  height, width, lay = img_tgt.shape
  reg = cv2.warpPerspective(img_src, h, (width, height))
  #print("Estimated homography : \n",  h)
  return reg, h


def full_basic_registration(img_tgt, img_src):
  kp_src, des_src = orb(img_src)
  kp_tgt, des_tgt = orb(img_tgt)
  matches = matcher(des_src,des_tgt)
  img_matches = cv2.drawMatches(img_src, kp_src, img_tgt, kp_tgt, matches, None)
  img_reg, h = transform(img_tgt, img_src, kp_tgt, kp_src, matches)
  return img_reg, img_matches, h

