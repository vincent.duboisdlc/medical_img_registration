#!/usr/bin/env python

import cv2
import matplotlib.pyplot as plt
import reglib as reg
import numpy as np

# Debug
#import pdb; pdb.set_trace()


def main():
  mat_US1 = cv2.imread('./data/tmp/US.jpg')
  mat_US0 = cv2.imread('./data/tmp/US0.jpg')
  mat_RMI = cv2.imread('./data/tmp/RMI.jpg')

  mat_reg1, mat_matches1, h = reg.full_basic_registration(mat_US0, mat_US1)
  h0 = np.load('./data/tmp/H0.npy')
  height, width, layes = mat_RMI.shape
  mat_reg2 = cv2.warpPerspective(mat_reg1, h0, (width, height))

  plt.subplot(231)
  plt.title('US0')
  plt.imshow(mat_US0, cmap='gray')
  cv2.imwrite('./US0.jpg', mat_US0)
  plt.subplot(232)
  plt.title('US1')
  plt.imshow(mat_US1, cmap='gray')
  cv2.imwrite('./US1.jpg', mat_US1)
  plt.subplot(233)
  plt.title('RMI')
  plt.imshow(mat_RMI, cmap='gray')
  cv2.imwrite('./RMI.jpg', mat_RMI)
  plt.subplot(234)
  plt.title('Matches1')
  plt.imshow(mat_matches1, cmap='gray')
  cv2.imwrite('./matches1.jpg', mat_matches1)
  plt.subplot(235)
  plt.title('Reg1')
  plt.imshow(mat_reg1, cmap='gray')
  cv2.imwrite('./reg1.jpg', mat_reg1)
  plt.subplot(236)
  plt.title('Reg final')
  plt.imshow(mat_reg2, cmap='gray')
  cv2.imwrite('./reg2.jpg', mat_reg2)

  plt.show()


if __name__ == '__main__':
  main()
